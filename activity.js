// Use the count operator to count the total number of fruits on sale
db.fruits.aggregate([
    {$match:{onSale: true}},
    {$count: "fruitsOnSale"}
]);
    
// Use the count operator to count the total number of fruits with stock more than or equal to 20.
db.fruits.aggregate([
    {$match:{stock: {$gte: 20}}},
    {$count: "enoughStock"}
]);
    
// Use the average operator to get the average price of fruits onSale per supplier
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avg_Price: {$avg: "$price"}}}
]);
    
// Use the max operator to get the highest price of a fruit onSale per supplier
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", max_Price: {$max: "$price"}}}
]);

// Use the min operator to get the lowest price of a fruit onSale per supplier
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", min_Price: {$min: "$price"}}}
]);